from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from store.models import Category, Company, Product
from store.serializers import CategorySerializer, CompanySerializer, ProductSerializer


class CategoryView(ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class CompanyView(ModelViewSet):
    queryset = Company.objects.filter(is_active=True)
    serializer_class = CompanySerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class ProductView(ModelViewSet):
    queryset = Product.objects.filter(is_active=True)
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filter_fields = ['company', 'category']
    search_fields = ['title']

    def create(self, request, *args, **kwargs):
        product_data = request.data
        new_product = Product.objects.create(title=product_data['title'], description=product_data['description'],
                                             category=Category.objects.get(id=product_data['category']),
                                             company=Company.objects.get(id=product_data['company']),
                                             is_active=product_data['is_active'])
        new_product.save()
        serializer = ProductSerializer(new_product)
        return Response(serializer.data)
