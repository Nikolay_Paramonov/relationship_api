from rest_framework.routers import SimpleRouter

from store.views import CompanyView, CategoryView, ProductView

router = SimpleRouter()

router.register(r'company', CompanyView)
router.register(r'category', CategoryView)
router.register(r'product', ProductView)


urlpatterns = [

]

urlpatterns += router.urls
