from django.db import models
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField


class CustomUser(AbstractUser):
    phone = PhoneNumberField(blank=True)


class Category(models.Model):
    title = models.CharField(max_length=64)

    def __str__(self):
        return self.title


class Company(models.Model):
    description = models.TextField()
    is_active = models.BooleanField()

    def __str__(self):
        return self.description


class Product(models.Model):
    title = models.CharField(max_length=64)
    description = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    is_active = models.BooleanField()

    def __str__(self):
        return self.title
