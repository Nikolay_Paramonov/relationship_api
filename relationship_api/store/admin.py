from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm
from store.models import Category, Company, Product, CustomUser

admin.site.register(Category)
admin.site.register(Company)
admin.site.register(Product)


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    model = CustomUser
    list_display = ['username', 'email', 'first_name', 'last_name', 'is_staff', 'phone']
    fieldsets = (
        *UserAdmin.fieldsets,
        (
            'Phone number',
            {
                'fields': (
                    'phone',
                )
            }
        )
    )


admin.site.register(CustomUser, CustomUserAdmin)

